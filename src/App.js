import React from 'react'
import './App.css';
import {Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FormBlock } from './common-components';
import Routes from './routes';

function App() {
  return (
    <div className="App">
      <Container fluid>
      this is containers
      {/* <FormBlock /> */}
      <Routes />
      </Container>
    </div>
  );
}

export default App;
