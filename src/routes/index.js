import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";


import { Login } from "../features";

export default function Routes() {
  return (
    <Router>
      <div  className="routes">
        {/* <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/topics">Topics</Link>
          </li>
        </ul> */}

        <Switch>
          <Route path="/react/login">
            <Login />
          </Route>
          <Route path="/react/about">
            <div > this is about </div>
          </Route>
          <Route path="/react/topics">
            <div > this is topics </div>
          </Route>
          <Route path="/react/">
            <div > this is Home </div>
          </Route>
          
        </Switch>
      </div>
    </Router>
  );
}